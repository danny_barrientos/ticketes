Meta:

Narrative:
As a user of despegar
I want to search a flight with my specifications
So that I can choose the best option for me

Scenario: search flight with same origin and destiny
Given I am on despegar.com.co home's page
When I fill search fields
|from                         |to                                                    |
|Medellín, Antioquia, Colombia|Aeropuerto Rafael Nunez, Cartagena de Indias, Colombia|
And click on search button
Then system show a same city error notification below destiny field
