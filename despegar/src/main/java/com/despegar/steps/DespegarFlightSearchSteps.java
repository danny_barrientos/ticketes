package com.despegar.steps;


import com.despegar.pages.DespegarHomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

public class DespegarFlightSearchSteps extends ScenarioSteps{

    @Page
    private DespegarHomePage despegarHomePage;

    @Step
    public void openPage(){
        despegarHomePage.open();
       despegarHomePage.closePopUP();
    }

    @Step
    public void fillSearchFields(ExamplesTable searchDataTable){
        despegarHomePage.fillSearchFields(searchDataTable);
    }

    @Step
    public void searchFlight(){
        despegarHomePage.searchFlight();
    }

    @Step
    public void verifySameCity(){
        despegarHomePage.verifySameCity();
    }

    @Step
    public void verifyOnlyDestiny(){
        despegarHomePage.verifyOnlyDestiny();
    }

    @Step
    public void verifyOnlyOrigin(){
        despegarHomePage.verifyOnlyOrigin();
    }
}
