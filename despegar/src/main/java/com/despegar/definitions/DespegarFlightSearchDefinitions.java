package com.despegar.definitions;

import com.despegar.steps.DespegarFlightSearchSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

public class DespegarFlightSearchDefinitions {
    @Steps
    private DespegarFlightSearchSteps despegarsFlightSearchSteps;

    @Given("I am on despegar.com.co home's page")
    public void openPage() {
        despegarsFlightSearchSteps.openPage();
    }

    @When("I fill search fields $searchDataTable")
    public void fillSearchFields(ExamplesTable searchDataTable) {
       despegarsFlightSearchSteps.fillSearchFields(searchDataTable);
    }

    @When("click on search button")
    public void searchFlight() {
        despegarsFlightSearchSteps.searchFlight();
    }

    @Then("system show a same city error notification below destiny field")
    public void verifySameCity() {
        //despegarsFlightSearchSteps.verifySameCity();
    }

    @Then("system show a origin is empty error notification below origin field")
    public void verifyOnlyDestiny() {
        //despegarsFlightSearchSteps.verifyOnlyDestiny();
    }

    @Then("system show a destiny is empty error notification below origin field")
    public void verifyOnlyOrigin() {
        //despegarsFlightSearchSteps.verifyOnlyOrigin();
    }

}
