package com.despegar.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebDriver;

import java.util.Map;

@DefaultUrl("https://www.despegar.com.co/vuelos/")
public class DespegarHomePage extends PageObject {


    @FindBy(xpath = "//span[@class='as-login-close as-login-icon-close-circled']")
    private  WebElementFacade popUp;

    @FindBy(xpath = "//input[@class='input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline']")
    private WebElementFacade originField;

    @FindBy(xpath = "//input[@class='input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-destination-input sbox-secondary sbox-places-second places-inline']")
    private WebElementFacade destinyField;;


    @FindBy(xpath = "//div[@class='sbox-3-btn -secondary -md sbox-search' and contains(., 'Buscar')]")
    private WebElementFacade searchButton;

    @FindBy(id = "departure-date-show")
    private WebElementFacade departureDateField;

    @FindBy(id = "return-date-show")
    private WebElementFacade returnDateField;

    @FindBy(className = "tooltip")
    private WebElementFacade errorTooltip;

    public DespegarHomePage(WebDriver driver){
        super(driver);
    }

    @WhenPageOpens
    public void waitUntilMainElementsAppears() {getDriver().manage().window().maximize();}

    public void closePopUP() {
        //popUp.and().waitUntilVisible().click();
        popUp.waitUntilPresent().waitUntilVisible().click();
    }
    public void fillSearchFields(ExamplesTable searchDataTable){
        Map<String, String> searchData = searchDataTable.getRow(0);
        String from=searchData.get("from");
        String to=searchData.get("to");

        originField.typeAndEnter(from);
        destinyField.typeAndEnter(to);


    }

    public void searchFlight(){
        searchButton.click();
    }

    public void verifySameCity(){
        String sameCityText = errorTooltip.getText();
        MatcherAssert.assertThat("User can't continue with the same city on origin and destiny fields",sameCityText.equals("El destino debe ser diferente del origen."));
    }

    public void verifyOnlyDestiny(){
        String sameCityText = errorTooltip.getText();
        MatcherAssert.assertThat("User can't continue with the same city on origin and destiny fields",sameCityText.equals("Ingrese un aeropuerto de origen."));
    }

    public void verifyOnlyOrigin(){
        String sameCityText = errorTooltip.getText();
        MatcherAssert.assertThat("User can't continue with the same city on origin and destiny fields",sameCityText.equals("Ingrese un aeropuerto de destino."));
    }


}
